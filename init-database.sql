USE [Sample]
GO
SET NOCOUNT ON

-- We'll create a simple view to give us a complete list of the correlation ids
IF OBJECT_ID('v_CorrelationIds') IS NOT NULL DROP VIEW v_CorrelationIds
GO
CREATE VIEW v_CorrelationIds
AS
SELECT CorrelationId FROM Table1 UNION
SELECT CorrelationId FROM Table2 UNION
SELECT CorrelationId FROM Table3 
GO

-- And we'll need some tables to hold our data
IF OBJECT_ID('Table1') IS NOT NULL DROP TABLE Table1
CREATE TABLE Table1 (
	Id INT NOT NULL IDENTITY,
	CorrelationId VARCHAR(25),
	FirstName VARCHAR(25),
	LastName VARCHAR(25),
	Address1 VARCHAR(25),
	Address2 VARCHAR(25)
)

CREATE NONCLUSTERED INDEX [Table1_CorrelationId] ON [dbo].[Table1]
(
	[CorrelationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]



IF OBJECT_ID('Table2') IS NOT NULL DROP TABLE Table2
CREATE TABLE Table2 (
	Id INT NOT NULL IDENTITY,
	CorrelationId VARCHAR(25),
	FirstName VARCHAR(25),
	LastName VARCHAR(25),
	Address1 VARCHAR(25),
	Address2 VARCHAR(25)
)

CREATE NONCLUSTERED INDEX [Table2_CorrelationId] ON [dbo].[Table2]
(
	[CorrelationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]


IF OBJECT_ID('Table3') IS NOT NULL DROP TABLE Table3
CREATE TABLE Table3 (
	Id INT NOT NULL IDENTITY,
	CorrelationId VARCHAR(25),
	FirstName VARCHAR(25),
	LastName VARCHAR(25),
	Address1 VARCHAR(25),
	Address2 VARCHAR(25)
)

CREATE NONCLUSTERED INDEX [Table3_CorrelationId] ON [dbo].[Table3]
(
	[CorrelationId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]

PRINT 'Created sample tables (dropped existing ones if needed)'


-- Lastly, we need to put some data in those tables
INSERT INTO [dbo].[Table1]([CorrelationId] ,[FirstName], [LastName], [Address1], [Address2])
SELECT 'A1', 'Nate', 'Bedford', '123 My Street', 'More' UNION ALL
SELECT 'A2', 'Jen', 'Bedford', '123 My Street', 'More' 

INSERT INTO [dbo].[Table2]([CorrelationId] ,[FirstName], [LastName], [Address1], [Address2])
SELECT 'A1', 'Nate', 'Bedford', '123 My Street', 'More' UNION ALL
SELECT 'A3', 'Estelle', 'Bedford', '123 My Street', 'More' 

INSERT INTO [dbo].[Table3]([CorrelationId] ,[FirstName], [LastName], [Address1], [Address2])
SELECT 'A1', 'Nate', 'Bedford', '123 My Street', 'More'

PRINT 'Inserted sample data'
SET NOCOUNT OFF