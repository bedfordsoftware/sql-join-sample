﻿using Dapper;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Dynamic;
using System.Linq;

namespace SqlJoins
{
    class Program
    {
        static void Main(string[] args)
        {
            var data = new DataRepository().GetAllData();

            // Web.API will automatically convert our output into JSON, but we can do it manually
            // to easily inspect the results:
            var jsonData = JsonConvert.SerializeObject(data);
            Debugger.Break();
        }
    }
}
