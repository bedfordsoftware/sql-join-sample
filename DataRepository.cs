﻿using Dapper;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace SqlJoins
{
    public class DataRepository
    {
        private List<string> tablePrefixes = new List<string>() { "tab1", "tab2", "tab3" };

        public List<Dictionary<string, object>> GetAllData()
        {
            var connectionString = @"Server=localhost\sqlexpress;Database=Sample;Trusted_Connection=True;";

            // notice we're using spaces to separate the tables in the SELECT list
            string sql = @"
                SELECT t1.*, '', t2.*, '', t3.*
                FROM v_CorrelationIds c
                LEFT OUTER JOIN Table1 t1 ON c.CorrelationId = t1.CorrelationId
                LEFT OUTER JOIN Table2 t2 ON c.CorrelationId = t2.CorrelationId
                LEFT OUTER JOIN Table3 t3 ON c.CorrelationId = t3.CorrelationId
                ";

            var finalResults = new List<Dictionary<string, object>>();

            using (var connection = new SqlConnection(connectionString))
            {
                // get all the data
                var rawData = connection.Query(sql).ToList();

                // we'll go through each row and map data to a new object and set the proper column prefix
                foreach (IDictionary<string, object> row in rawData)
                {
                    var mappedRow = this.MapDataRow(row);
                    finalResults.Add(mappedRow);
                }
            }

            return finalResults;
        }

        private Dictionary<string, object> MapDataRow(dynamic row)
        {
            var mappedRow = new Dictionary<string, object>();
            var currentPrefixIndex = 0;

            foreach (var field in row)
            {
                var tablePrefix = tablePrefixes.ElementAt(currentPrefixIndex);

                // In the query, we've separated the data from each table by an empty column.
                // This makes it easy for us to split up the data and assign the proper prefix.
                // Every time we see a column with an empty name, we know the next set of data will
                // be from the next table, so we increment our prefix counter
                if (string.IsNullOrEmpty(field.Key))
                {
                    currentPrefixIndex++;
                }
                else
                {
                    mappedRow[$"{tablePrefix}_{field.Key}"] = field.Value;
                }
            }

            return mappedRow;
        }
    }
}
