# SQL Join Sample

To run this locally, you'll need to:

- Create a database named "Sample" to run the init script against
- Run the "init-database.sql" script to create the sample data
- Update the connection string in the "DataRepository" class to use the correct server and database
- Open the SqlJoins.sln solution
- Run the program
